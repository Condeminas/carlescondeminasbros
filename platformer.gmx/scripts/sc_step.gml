
move = key_left + key_right;
hsp = move*movespeed;

if(vsp<10)vsp+=grav;

if(place_meeting(x,y+1,obj_wall))
{
    //Check if recently grounded
    if(!grounded && !key_jump){
        hkp_count = 0; //Init horizontal count
        jumping = false;
    }else if(grounded && key_jump){ //recently jumping
       
        jumping = true;
    }

    //Check if player grounded
    grounded = !key_jump;
    
    vsp = key_jump * -jumpspeed;
}

//Init hsp_jump_applied
if(grounded){
    hsp_jump_applied = 0;
}

if(grounded &&key_jump){
    audio_play_sound(snd_mario_jump, 10, false);
}
//Check horizontal counts
if(move!=0 && grounded){
 hkp_count++;
}else if(move==0 && grounded){
 hkp_count=0;
}

//Check jumping
if(jumping){
    
    //check if previously we have jump
    if(hsp_jump_applied == 0){
        hsp_jump_applied = sign(move);       
    }

    //don't jump horizontal
    if(hkp_count < hkp_count_small ){
        hsp = 0;
    }else if(hkp_count >= hkp_count_small && hkp_count < hkp_count_big){ //small jump
        hsp = hsp_jump_applied * hsp_jump_constant_small;
    }else{ // big jump
        hsp = hsp_jump_applied *hsp_jump_constant_big
    }
}

//horizontal collision
if(place_meeting(x+hsp,y,obj_wall))
{
    while(!place_meeting(x+sign(hsp),y,obj_wall))
    {
        x+= sign(hsp);
        move*= -1;
    }
    hsp = 0;
}

if(place_meeting(x-16,y,obj_wall)){
    move = 0;
    image_xscale=1;
}
if(place_meeting(x+16,y,obj_wall)){
    move=1;
    image_xscale=-1;
}

x+= hsp;
//vertical collision
if(place_meeting(x,y+vsp,obj_wall))
{
    while(!place_meeting(x, sign(vsp) + y, obj_wall))
    {
        y+= sign(vsp);
    }
    vsp = 0;
}

y+= vsp;


    
//wrap
if(x<=0-16){
    x=room_width;
}
else if(x>= room_width +16) {
    x=0;
}
//sprite change
if (grounded){
    if (hsp ==0){
        sprite_index = spr_mario_idle;
    } else {
        if(sprite_index != spr_mario_walking) image_index = 0;
        sprite_index = spr_mario_walking;
    }
}

//sprite spin
if(jumping) {
    sprite_index = spr_mario_jump;    
}
    
if (key_left == -1){
    image_xscale= -1;
}

if (key_right == 1){
    image_xscale= 1;
}

//gameover
if(global.lives==0){
    room_goto(rm_gameover);
}
